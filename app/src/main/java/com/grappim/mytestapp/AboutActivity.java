package com.grappim.mytestapp;

import android.app.Activity;
import android.os.Bundle;

public class AboutActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
  }
}
